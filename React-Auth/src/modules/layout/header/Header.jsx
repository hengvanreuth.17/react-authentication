import { useAuth } from "../../auth/components/Auth";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { Link } from "react-router-dom";

const Header = () => {
  const { logout } = useAuth();
  return (
    <>
      <header className=" p-2 d-flex bg-secondary">
        <Navbar expand="lg" className="container">
          <Container fluid>
            <Navbar.Brand href="#" className=" w-25 h-50"><img src="https://www.patterns.dev/img/reactjs/react-logo@3x.svg" alt=""  className=" w-25 h-50"/></Navbar.Brand>
            <Navbar.Toggle aria-controls="navbarScroll" />
            <Navbar.Collapse id="navbarScroll" className=" justify-content-between fs-5 text-light">
              <Nav variant="pills" defaultActiveKey="/home">
                <Nav.Item className=" px-3">
                  <Nav.Link href="/home" className=" text-light">Home</Nav.Link>
                </Nav.Item>
                <Nav.Item className=" px-3">
                  <Nav.Link eventKey="link-1" className=" text-light" as={Link} to="/Blog">Product</Nav.Link>
                </Nav.Item>
                <Nav.Item className=" px-3">
                  <Nav.Link eventKey="link-2 " className=" text-light" as={Link} to="/Contact" >
                    Contact
                  </Nav.Link>
                </Nav.Item>
              </Nav>
              <Button
                className=" btn btn-danger"
                onClick={() => {
                  logout();
                }}
              >
                LogOut
              </Button>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </header>
    </>
  );
};

export default Header;