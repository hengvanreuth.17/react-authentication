import React, { useState, useEffect } from 'react';
import { FaStar, FaStarHalfAlt } from "react-icons/fa";
import { AiOutlineStar } from "react-icons/ai";
import { getProductById } from '../../product/core/request'; // Assuming getProductById retrieves product by id
import { useParams } from "react-router-dom";

const Star = ({ stars }) => {
    const ratingStar = Array.from({ length: 5 }, (_, index) => {
        const number = index + 1;

        if (stars >= number) {
            return <FaStar key={index} />;
        } else if (stars >= number - 0.5) {
            return <FaStarHalfAlt key={index} />;
        } else {
            return <AiOutlineStar key={index} />;
        }
    });

    return (
        <div>
            {ratingStar}
        </div>
    );
};

const RatedProduct = () => {
    const [ratedProduct, setRatedProduct] = useState(null);
    const { id } = useParams();

    useEffect(() => {
        getProductById(id).then((response) => {
            setRatedProduct(response.data);
        }).catch(error => {
            console.error('Error fetching rated product:', error);
            setRatedProduct(null); // Reset ratedProduct state if error occurs
        });
    }, [id]);

    if (ratedProduct === null) {
        return <p>Loading...</p>; // Display loading message while fetching data
    }

    return (
        <div>
            {ratedProduct ? (
                <div key={ratedProduct.id}>
                    <p className=' text-warning d-flex gap-2'><Star stars={ratedProduct.rating} />({ratedProduct.rating})</p>
                </div>
            ) : (
                <p>No rated product found with ID: {id}</p>
            )}
        </div>
    );
};

export default RatedProduct;
