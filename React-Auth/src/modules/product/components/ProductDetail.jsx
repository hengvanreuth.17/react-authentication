import { Link, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { getProductById } from "../core/request";
import "bootstrap/dist/css/bootstrap.min.css";
import { ArrowBigLeft } from "lucide-react";
import Carousel from "react-bootstrap/Carousel";
import { Rating } from 'react-simple-star-rating'
import Dropdown from 'react-bootstrap/Dropdown';
import Star from "../../auth/components/Star";


const ProductDetail = () => {
  const { id } = useParams();
  const [product, setProduct] = useState({});
  console.log(product);

  useEffect(() => {
    getProductById(id).then((response) => {
      setProduct(response.data);
    });
  }, [id]);

  if (!product.id) {
    return <div className="text-center fs-6">Loading...</div>;
  }

  return (
    <div className="container">
      <div className="mx-auto">
        <button className=" btn btn-dark text-light my-3">
          <Link to="/" className=" text-light">
            <ArrowBigLeft />
          </Link>
        </button>
        <div className="row">
          <div className="col-lg-6 mb-4">
            <Carousel  data-bs-theme="dark">
              {product.images.map((image, index) => (
                <Carousel.Item key={index}>
                  <img
                    className="w-100"
                    src={image}
                    alt=""
                    style={{ height: "30rem" }}
                  />
                </Carousel.Item>
              ))}
            </Carousel>
          </div>
          <ul className=" col-lg-6  text-decoration-none list-unstyled ps-5 m-auto">
          <li className=" text-secondary">
            <div className=" d-flex justify-content-between">
              <p className=" fs-5">
               {product.brand}
              </p>
              <span className=" fs-6"><Star/></span>
              </div>
            </li>
            <li className=" ">
              <h3 className=" fw-bold">
              {product.title}
              </h3>
            </li>
            <li className="py-3">
              <h5>{product.description}</h5>
            </li>
            <li className="">
              <h3 className=" fw-bold text-warning">
                  ${(product.price - (product.price * (product.discountPercentage / 100))).toFixed(2)}  
                  <span className=" fs-5 ms-2 text-danger bg-danger-subtle rounded-1 p-2">
                    {product.discountPercentage} %
                  </span>
              </h3>
            </li>
            <li className="">
              <h4 className=" d-flex">
                <span className="text-decoration-line-through text-danger">${product.price}</span><p className=" fs-6 mt-2 ps-2">(Stoky:{product.stock})</p>
              </h4>
            </li>
            
          </ul>
        </div>
      </div>
    </div>
  );
};
export default ProductDetail;
