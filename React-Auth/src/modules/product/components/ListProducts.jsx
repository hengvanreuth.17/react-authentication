import { useEffect, useState } from "react";
import { getCategories, getListProduct, getListProductByCategory, searchProduct } from "../core/request";
import { useNavigate } from "react-router-dom";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Badge from 'react-bootstrap/Badge';
import { Link } from "react-router-dom";

import PaginationBasic from "./Pagination";
import { BadgeCent } from "lucide-react";


const ListProducts = () => {

    const [products, setProducts] = useState([]);
    const [active, setActive] = useState("All");
    const [categories, setCategories] = useState([]);
    const [searchText, setSearchText] = useState("");
    const [total, setTotal] = useState(0);
    const navigate = useNavigate();

    const limit = 12;

    useEffect(() => {
        getListProduct(limit).then((response) => {
            setProducts(response.data.products);
            setTotal(response.data.total);
        });

        getCategories().then((response) => {
            setCategories(response.data);
        })
    }, []);


    useEffect(() => {
        active === "All" ?
            getListProduct(limit).then((response) => {
                setProducts(response.data.products);
                setTotal(response.data.total);
            })
            : getListProductByCategory(limit, active).then((response) => {
                setProducts(response.data.products);
                setTotal(response.data.total);

            });
    }, [active]);


    const onSearch = () => {
        searchProduct({ q: searchText }).then((response) => {
            setProducts(response.data.products);
        });
    }




    return <div className="container mt-5">
        <div className="w-100 d-flex justify-content-center mb-4">
            <Form className="d-flex justify-content-center px-5 w-50">
                <Form.Control
                    type="search"
                    placeholder="Search"
                    className="me-2"
                    aria-label="Search"
                    onChange={(e) => setSearchText(e.target.value)}
                />
                <Button onClick={onSearch} variant="outline-success">Search</Button>
            </Form>
        </div>

        <div className="px-4">
            <div className="d-flex overflow-x-scroll w-100 pb-3">
                <Badge onClick={() => setActive("All")} className="px-4 py-2 fs-6 mx-2" bg={active === "All" ? "primary" : "secondary"}>All</Badge>
                {categories.map((item, index) =>
                    <Badge onClick={() => setActive(item)} key={index} className="px-4 py-2 fs-6 mx-2 " bg={active === item ? "primary" : "secondary"}>{item}</Badge>)}
            </div>
        </div>


        <div className="d-flex flex-wrap justify-content-center">
    {products.length === 0 && <div>Not Found</div>}
    {products.map(({ id, thumbnail, title, price,discountPercentage }) => {
        return (
            <div  className="mx-2 border rounded-3 my-3 position-relative" key={id}>
                <div>
                <div className="position-absolute end-0  text-danger"><BadgeCent /></div>
                <img style={{ height: "200px", width: "300px" }}  src={thumbnail} alt={title}
                />
                </div>
                <p className="text-center pt-2 fs-5 fw-bold">{title}</p>
                <div className="d-flex justify-content-between px-4 pb-3 fw-bold">
                    <div className=" d-flex">
                    <p className="text-danger text-decoration-line-through">{price}$</p>
                    <p className="text-danger ps-2">{(price - (price * (discountPercentage / 100))).toFixed(2)}$</p>
                    </div>
                    <button  onClick={() => navigate(`product/${id}`)} className="btn btn-secondary" to={id.toString()}>Detail</button>
                </div>
            </div>
        );
    })}
</div>
        <div className="d-flex justify-content-center my-3">
            <PaginationBasic onChangePage={(page) => {
                getListProduct(limit, (page - 1) * limit).then((response) => {
                    setProducts(response.data.products);
                    setTotal(response.data.total);
                });

            }} total={total / limit} />
        </div>
    </div>
}

export default ListProducts;