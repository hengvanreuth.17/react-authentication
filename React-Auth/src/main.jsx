import React from 'react'
import ReactDOM from 'react-dom/client'
import {AuthInit} from './modules/auth/components/AuthInit'
import { AuthProvider } from './modules/auth/components/Auth'
import App from './App'
import 'bootstrap/dist/css/bootstrap.css'
import './index.css'
import { setUpAxios } from './modules/auth/components/AuthHelper';


const root = ReactDOM.createRoot(document.getElementById('root'));
setUpAxios();
root.render(
    <React.StrictMode>
        <AuthProvider>
            <AuthInit>
                <App />
            </AuthInit>
        </AuthProvider>
    </React.StrictMode>
);

